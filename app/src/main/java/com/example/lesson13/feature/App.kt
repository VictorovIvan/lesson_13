package com.example.lesson13.feature

import android.app.Application
import com.example.lesson13.feature.city_screen.di.cityScreenModule
import com.example.lesson13.feature.weather_screen.di.weatherScreenModule
import com.example.lesson13.feature.wind_screen.di.windScreenModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(appModule, weatherScreenModule, cityScreenModule, windScreenModule)
        }
        Timber.plant(Timber.DebugTree())
    }
}