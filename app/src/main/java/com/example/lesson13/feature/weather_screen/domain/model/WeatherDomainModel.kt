package com.example.lesson13.feature.weather_screen.domain.model

data class WeatherDomainModel(
    val temperature: Float,
    val temperatureMax: Float,
    val temperatureMin: Float,
    val humidity: Int,
    val windDeg: Float
)