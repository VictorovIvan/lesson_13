package com.example.lesson13.feature

import androidx.lifecycle.ViewModel

class MainPresenter : ViewModel() {

    fun getTemperature(): String {
        return "32"
    }
}