package com.example.lesson13.feature.weather_screen.ui

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.lesson13.R
import com.example.lesson13.feature.weather_screen.domain.model.WeatherDomainModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherScreenActivity : AppCompatActivity() {

    private val weatherViewModel by viewModel<WeatherScreenViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_weather_screen)

        val btnGet = findViewById<Button>(R.id.btnGet)

        weatherViewModel.weatherLiveData.observe(this, Observer(::render))

        btnGet.setOnClickListener {
            weatherViewModel.requestWeather()
        }
    }

    private fun render(state: WeatherDomainModel) {
        findViewById<TextView>(R.id.tvTemperature).text = "Температура: ".plus(state.temperature).plus(" градусов")
        findViewById<TextView>(R.id.tvTemperatureMax).text = "Максимальная температура: ".plus(state.temperatureMax).plus(" градусов")
        findViewById<TextView>(R.id.tvTemperatureMin).text = "Минимальная температура: ".plus(state.temperatureMin).plus(" градусов")
        findViewById<TextView>(R.id.tvHumidity).text = "Влажность: ".plus(state.humidity).plus("%")
    }
}