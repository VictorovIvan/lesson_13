package com.example.lesson13.feature.weather_screen.data.api

import com.example.lesson13.feature.weather_screen.data.api.WeatherRemoteSource
import com.example.lesson13.feature.weather_screen.data.api.WeatherRepo
import com.example.lesson13.feature.weather_screen.data.toDomain
import com.example.lesson13.feature.weather_screen.domain.model.WeatherDomainModel

class WeatherRepoImp(
    private val weatherRemoteSource: WeatherRemoteSource
) : WeatherRepo {

    override suspend fun getWeather(cityName: String): WeatherDomainModel {
        return weatherRemoteSource.getWeather(cityName).toDomain()
    }
}