package com.example.lesson13.feature.city_screen.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson13.R
import org.koin.androidx.viewmodel.ext.android.viewModel

class CityScreenActivity : AppCompatActivity() {

    private val cityScreenViewModel by viewModel<CityScreenViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cityScreenViewModel.setCityName(CURRENT_CITY)
    }

    companion object {
        const val CURRENT_CITY = "Innopolis"
    }
}