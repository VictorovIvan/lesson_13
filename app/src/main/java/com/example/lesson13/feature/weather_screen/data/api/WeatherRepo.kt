package com.example.lesson13.feature.weather_screen.data.api

import com.example.lesson13.feature.weather_screen.domain.model.WeatherDomainModel

interface WeatherRepo {

    suspend fun getWeather(cityName:String): WeatherDomainModel
}