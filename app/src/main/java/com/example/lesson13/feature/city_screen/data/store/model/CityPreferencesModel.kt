package com.example.lesson13.feature.city_screen.data.store.model

data class CityPreferencesModel(
    val cityName: String
)