package com.example.lesson13.feature

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson13.R
import com.example.lesson13.feature.weather_screen.ui.WeatherScreenActivity
import com.example.lesson13.feature.wind_screen.ui.WindScreenActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val weatherButton = findViewById<Button>(R.id.weatherButton)
        val windButton = findViewById<Button>(R.id.windButton)

        weatherButton.setOnClickListener {
            Intent(this, WeatherScreenActivity::class.java).also {
                startActivity(it)
            }
        }

        windButton.setOnClickListener {
            Intent(this, WindScreenActivity::class.java).also {
                startActivity(it)
            }
        }
    }
}